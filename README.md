# ublock-rabbit-hole-news

Block links in the page that invite you to consume more news and articles, like
"most popular", "most read", news carousels, chatbox, and other distractions 
that appear on news sites while reading an article.

In uBlock Origin, go to the tab Filter Lists, and at the bottom click on 
"Import..." and paste this:

```
https://gitlab.com/gasull/ublock-rabbit-hole-news/raw/master/rules.txt
```